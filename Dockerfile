FROM python:3.9-slim
ENV PYTHONUNBUFFERED=1
WORKDIR /production_site

COPY . .
RUN pip3 install -r requirements.txt

CMD python3 my_page/manage.py runserver 0.0.0.0:8001

