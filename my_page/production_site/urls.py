from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='production-site-index'),
    path('ddd/', views.get_info_about_site, name='info-site'),
]
