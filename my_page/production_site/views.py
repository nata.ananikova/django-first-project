from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from django.urls import reverse
from dataclasses import dataclass
from django.template.loader import render_to_string


# Create your views here.

class Product:

    def __init__(self, name_batch, granular_type=None, granular_site=None,
                 quantity_granular=None, granular_weight=None, tablet_site=None, sache_site=None,
                 quantity_tablet=None, quantity_saches=None, coating_site=None, capsule_site=None,
                 quantity_coated=None, quantity_capsule=None, blister_site=None, jar_site=None,
                 quantity_blister=None, quantity_jar=None):
        self.name_batch = name_batch
        self.granular_type = granular_type
        self.granular_site = granular_site
        self.quantity_granular = quantity_granular
        self.granular_weight = granular_weight
        self.tablet_site = tablet_site
        self.sache_site = sache_site
        self.quantity_tablet = quantity_tablet
        self.quantity_saches = quantity_saches
        self.coating_site = coating_site
        self.capsule_site = capsule_site
        self.quantity_coated = quantity_coated
        self.quantity_capsule = quantity_capsule
        self.blister_site = blister_site
        self.jar_site = jar_site
        self.quantity_blister = quantity_blister
        self.quantity_jar = quantity_jar

    def get_info(self, name_batch):
        return self.granular_type, self.granular_site, self.quantity_granular, self.granular_weight, self.tablet_site, \
               self.sache_site, self.quantity_tablet, self.quantity_saches, self.coating_site, self.capsule_site, \
               self.quantity_coated, self.quantity_capsule, self.blister_site, self.jar_site, self.quantity_blister, \
               self.quantity_jar


production_sites = ['TMP3', 'TMP2', 'TMP1', 'clad_new', 'clad_old', 'clad_tabl', 'tablet1', 'tablet2',
                    'blister1', 'blister2', 'jar2', 'tablet3', 'sache', 'washing', 'clad_coating', 'coating1',
                    'capsule1', 'capsule2', 'jar1', 'coating2', 'chemistry']

products = [Product(name_batch='velsone_1', granular_type='dry', granular_site=production_sites[0],
                    quantity_granular=1, granular_weight=80, tablet_site=production_sites[7],
                    sache_site=None, quantity_tablet=79, quantity_saches=None,
                    coating_site=production_sites[20], capsule_site=None,
                    quantity_coated=81, quantity_capsule=None, blister_site=None,
                    jar_site=None, quantity_blister=None, quantity_jar=None),

            Product(name_batch='drotaverine_1', granular_type='wet', granular_site=production_sites[0],
                    quantity_granular=3, granular_weight=204, tablet_site=production_sites[7],
                    sache_site=None, quantity_tablet=199, quantity_saches=None,
                    coating_site=None, capsule_site=None,
                    quantity_coated=None, quantity_capsule=None, blister_site=None,
                    jar_site=production_sites[10], quantity_blister=None, quantity_jar=11000),

            Product(name_batch='pankreatine_1', granular_type='wet', granular_site=production_sites[2],
                    quantity_granular=4, granular_weight=260, tablet_site=production_sites[6],
                    sache_site=None, quantity_tablet=258, quantity_saches=None,
                    coating_site=production_sites[15], capsule_site=None,
                    quantity_coated=263, quantity_capsule=None, blister_site=None,
                    jar_site=production_sites[18], quantity_blister=None, quantity_jar=13000),

            Product(name_batch='anvimax_1', granular_type='wet', granular_site=production_sites[1],
                    quantity_granular=5, granular_weight=500, tablet_site=None,
                    sache_site=production_sites[12], quantity_tablet=None, quantity_saches=100000,
                    coating_site=None, capsule_site=None,
                    quantity_coated=None, quantity_capsule=None, blister_site=None,
                    jar_site=None, quantity_blister=None, quantity_jar=None),


            Product(name_batch='vinpocetine_1', granular_type='dry', granular_site=production_sites[0],
                    quantity_granular=1, granular_weight=240, tablet_site=production_sites[7],
                    sache_site=None, quantity_tablet=235, quantity_saches=None,
                    coating_site=None, capsule_site=None,
                    quantity_coated=None, quantity_capsule=None, blister_site=None,
                    jar_site=None, quantity_blister=None, quantity_jar=None)]


def index(request):
    return render(request, 'production_site/index.html', context={
        'production_sites': production_sites,
    })


def get_info_about_site(request, name_site: str):
    for i in production_sites:
        name_site = i
    return render(request, 'production_site/info_site.html', context={
            'name_batch': 'name_batch',
            'info': 'info'
            })



