asgiref==3.4.1
dataclasses==0.6
Django==3.2.9
django-extensions==3.2.0
pytz==2021.3
sqlparse==0.4.2
typing-extensions==3.10.0.2
